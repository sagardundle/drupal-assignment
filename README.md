# Developed using drupal 9.1.0

## Steps to Install
    * Composer Install
    * Import database from db folder
    * Check config form custom module is enabled or not
    * For admin setting, navigate to configuration->Admin Custom Configurations

## Admin Config Form
![picture](img/admin.png)
![picture](img/admin1.PNG)
![picture](img/admin2.PNG)

## Home page
![picture](img/home.PNG)

