<?php

namespace Drupal\config_form\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\config_form\Controller\CustomConfigController;


/**
 * Provides a 'Custom' Block.
 *
 * @Block(
 *   id = "custom_block",
 *   admin_label = @Translation("Custom Block"),
 *   category = @Translation("Custom Block"),
 * )
 */
class CustomBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
    public function build() {

      $data = CustomConfigController::display();
      $offset = explode('|', $data['#timezone']);

      return [
        '#markup' => '<b class="custom">Location: '.$data['#country'].', '.$data['#city'].', <span class="datetime">'.$data['#time'].'</span></b>',
        '#cache' => [
          'max-age' => 5000,
        ],
        '#attached' => [
          'library' => [
            'config_form/custom_library',
          ],
          'drupalSettings' => [
            'timezone_clock' => [
              'timezone' => $data['#timezone'],
              'offset' => $offset[1],
            ],
          ],
        ],
      ];

      
    }

}