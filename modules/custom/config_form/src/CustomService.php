<?php

namespace Drupal\config_form;

use DateTime;
use DateTimeZone;

/**
 * Custom service to get datetime according
 * to the timezone passed
 */

class CustomService{

    public function getTime($time)
    {
        $time = explode('|', $time);
        $date = new DateTime(date('Y-m-d H:i'), new DateTimeZone('Asia/Kolkata'));

        $date->setTimezone(new DateTimeZone($time[0]));
        return $date->format('jS M Y H:i A') . "\n";
    }

}