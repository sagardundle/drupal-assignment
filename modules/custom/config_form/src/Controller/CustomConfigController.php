<?php

namespace Drupal\config_form\Controller;

use Drupal\Core\Controller\ControllerBase;

class CustomConfigController extends ControllerBase{

    public static function display()
    {
        
        $country = \Drupal::config('config_form.settings')->get('country');
        $city = \Drupal::config('config_form.settings')->get('city');
        $timezone = \Drupal::config('config_form.settings')->get('timezone');
        $time = \Drupal::service('config_form.custom_service')->getTime($timezone);

        return [
            '#theme' => 'admin_template',
            '#country' => $country,
            '#city' => $city,
            '#timezone' => $timezone,
        ];
    }
    
}