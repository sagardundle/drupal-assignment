<?php

namespace Drupal\config_form\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class AdminForm extends ConfigFormBase{

    /** 
   * Config settings.
   *
   * @var string
   */
     const SETTINGS = 'config_form.settings';


    /** 
   * {@inheritdoc}
   */
    public function getFormId(){
        return 'config_form_custom';
    }


    /** 
   * {@inheritdoc}
   */
    public function getEditableConfigNames()
    {
        return [
            static::SETTINGS,
          ];
    }


    /** 
   * {@inheritdoc}
   */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config(static::SETTINGS);
        $country = \Drupal::config('config_form.settings')->get('country');
        $city = \Drupal::config('config_form.settings')->get('city');
        $timezone = \Drupal::config('config_form.settings')->get('timezone');
        $time = \Drupal::service('config_form.custom_service')->getTime($timezone);

        /* $timezones = [
            '0' => 'Select Timezone',
            '-06:00' => 'America/Chicago',
            '-05:00' => 'America/New York',
            '+09:00' => 'Asia/Tokio',
            '+04:00' => 'Asia/Dubai',
            '+05:30' => 'Asia/Kolkata',
            '+02:00' => 'Asia/Amsterdam',
            '+01:00' => 'Asia/Oslo',
            '+00:00' => 'Asia/London',
        ]; */

        $timezones = [
            '0' => 'Select Timezone',
            'America/Chicago|-6' => 'America/Chicago',
            'America/New_York|-5 ' => 'America/New York',
            'Asia/Tokyo|9' => 'Asia/Tokyo',
            'Asia/Dubai|4' => 'Asia/Dubai',
            'Asia/Kolkata|5.3' => 'Asia/Kolkata',
            'Europe/Amsterdam|1' => 'Europe/Amsterdam',
            'Europe/Oslo|1' => 'Europe/Oslo',
            'Europe/London|0' => 'Europe/London',
        ];

        $form['config'] = [
            '#type' => 'details',
            '#title' => t('Admin Configuration'),
            '#open' => TRUE,
        ];

        $form['config']['country'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Enter Country'),
            '#default_value' => ($country) ? $country : '',
            '#required' => FALSE,
        ];  

        $form['config']['city'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Enter City'),
            '#default_value' => ($city) ? $city : '',
            '#required' => FALSE,
        ];  

        $form['config']['timezone'] = [
            '#type' => 'select',
            '#title' => $this->t('Select Timezone'),
            '#options' => $timezones,
            '#default_value' => ($timezone) ? $timezone : '0',
            '#required' => FALSE,
            '#ajax' => [
                'callback' => '::getTimeZone',
                'event' => 'change',
                'progress' => [
                    'type' => 'throbber',
                    'message' => 'Please Wait',
                ],
            ],
        ];  

        $form['config']['markup'] = [
            '#type' => 'markup',
            '#markup' => '<b>Timezone is:</b> <div class="result_message"><span class="red">'.$time ?? ''.'</span></div>',
            '#attributes' => [
                'id' => 'custom-result',
              ],
        ];

        $form['#attached']['library'][] = 'config_form/custom_admin_library';

        return parent::buildForm($form, $form_state);
        
    }

 
    /** 
   * {@inheritdoc}
   */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $this->configFactory->getEditable(static::SETTINGS)
                         ->set('country', Html::escape($form_state->getValue('country')))
                         ->set('city', Html::escape($form_state->getValue('city')))
                         ->set('timezone', Html::escape($form_state->getValue('timezone')))
                         ->save(TRUE);
        // flush drupal cache
        drupal_flush_all_caches();
 
        return parent::submitForm($form, $form_state);
    }


    /**
     * return convert date time as per timezone selected
     */
    public function getTimeZone(array &$form, FormStateInterface $form_state)
    {
        $ajaxResponse = new AjaxResponse();
        $timezone = Html::escape($form_state->getValue('timezone'));
        $timezone = explode('|', $timezone);
        if($timezone[0] == '0'){
            $time = 'Please select Timezone';
        }else{
            $time = \Drupal::service('config_form.custom_service')->getTime($timezone[0]);
        }
        $text = '<span class="red">'.$time.'</span>';
        $ajaxResponse->addCommand(new InvokeCommand('.result_message', 'html', [ $text ]));
        return $ajaxResponse;
    }
    
}